package Homemaker;

use 5.010;
use strict;
use warnings;
use Carp;
use File::Spec::Functions qw/ splitdir rel2abs catdir catfile/;
use File::Path 'make_path';
use File::Temp 'tempdir';
use File::Copy;
use File::Copy::Recursive 'dircopy';
use Sys::Hostname;
use Cwd 'cwd';

=head1 NAME

Homemaker - Adaptive design queue manager

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';


=head1 SYNOPSIS

Queue manager for L<Urb>

Evolving a house using urb involves retrieving, evolving and updating a central data store with results.
These jobs will happen many times and in parallel.
This module uses files in a directory for keeping track of these jobs.
Using files for queue management allows jobs to be taken by multiple processes in parallel on the same machine or on different machines on a network.
Tasks can be killed or respawned at any time without breaking the job as a whole.

  use Homemaker;

=head1 METHODS

=over

=item new

Create a new Homemaker instance:

  export HOMEMAKER_QUEUE=/path/to/queue
  export HOMEMAKER_DATA=/path/to/data
  my $homemaker = Homemaker->new() || die;

=cut

sub new
{
    my $class = shift;
    my $self = {};
    return 0 unless (defined $ENV{HOMEMAKER_QUEUE} and defined $ENV{HOMEMAKER_DATA});
    $self->{path_queue} = rel2abs ($ENV{HOMEMAKER_QUEUE});
    $self->{path_data}  = rel2abs ($ENV{HOMEMAKER_DATA});
    make_path ($self->{path_queue}) unless -d $self->{path_queue};
    make_path ($self->{path_data}) unless -d $self->{path_data};

    bless $self, $class;
    return $self;
}

=item path_temp

Create a temporary folder on the local machine:

  $path_temp = $homemaker->path_temp;

=cut

sub path_temp
{
    my $self = shift;
    return tempdir (CLEANUP => 1);
}

=item number_generations number_iterations timeout

Query the number of generations to attempt (defaults to 16), or number iterations in a generation (defaults to 128), or maximum seconds for a generation:

  $number_generations = $homemaker->number_generations;
  $number_iterations = $homemaker->number_iterations;
  $timeout = $homemaker->timeout;

=cut

sub number_generations
{
    my $self = shift;
    return 16 unless defined $ENV{NUMBER_GENERATIONS};
    return $ENV{NUMBER_GENERATIONS};
}

# FIXME MAX_ITERATIONS or NUMBER_ITERATIONS
sub number_iterations
{
    my $self = shift;
    return 768 unless defined $ENV{NUMBER_ITERATIONS};
    return $ENV{NUMBER_ITERATIONS};
}

sub timeout
{
    my $self = shift;
    return 28800 unless defined $ENV{EVOLVE_TIMEOUT};
    return $ENV{EVOLVE_TIMEOUT};
}

=item plots

Get a list of plots, e.g for when we have a new project and want to create some jobs.

Looks for 'init.dom' in any sub-folders of 'my project/'

  @plots = $homemaker->plots ('my project');

=cut

sub plots
{
    my $self = shift;
    my $project = shift;
    my $path_project = catdir ($self->{path_data}, $project);
    opendir (DIR, $path_project);
    my @plots = ();
    while (my $name = readdir (DIR))
    {
        next if $name =~ /^\.\.?$/x;
        my $path_plot = catdir ($path_project, $name);
        next unless -d $path_plot;
        my $path_init = catfile ($path_plot, 'init.dom');
        next unless -e $path_init;
        $name =~ s/\/$//x;
        push @plots, $name;
    }
    closedir DIR;
    return @plots;
}

=item walls

Get a list of paths to *.walls files in a project:

  @path_walls = $homemaker->walls ($project);

=cut

sub walls
{
    my $self = shift;
    my $project = shift;
    my $path_project = catdir ($self->{path_data}, $project);
    opendir (DIR, $path_project);
    my @walls = ();
    while (my $name = readdir (DIR))
    {
        next unless $name =~ /\.walls$/x;
        push @walls, catfile ($path_project, $name);
    }
    closedir DIR;
    return @walls;
}

=item best

Get a .dom file and score sorted by 'best'

  ($path_best, $score_best) = $homemaker->best ('/path/to/plot/');

=cut

sub best
{
    my $self = shift;
    my $path_plot = shift;

    my $lookup = {};
    opendir (DIR, $path_plot);
    while (my $name = readdir (DIR))
    {
        next unless $name =~ /\.score$/x;
        open my $SCORE, '<', catdir ($path_plot, $name) or next;
        my @score = (<$SCORE>);
        my $score = shift @score;
        close $SCORE;
        $lookup->{$score} = $name;
    }
    closedir DIR;
    my @scores = sort {$b <=> $a} keys %{$lookup};
    my $best = $lookup->{$scores[0]};
    chomp $scores[0];
    $best =~ s/\.score$//x;
    return catdir ($path_plot, $best), $scores[0];
}

=back

=head1 TASKS

=over

=item prep

Take a prepared project folder and insert it for processing.
Iterate through the plots in a project, delete *.scores, copy init.dom file to best.dom

  $project = $homemaker->prep ('/local/path/to/my project');

($project will be 'my project')

=cut

sub prep
{
    my $self = shift;
    my $path_local = shift;
    $path_local =~ s/\/$//x;

    my @dirs = splitdir ($path_local);
    my $project = $dirs[-1];
    my $path_project = catdir ($self->{path_data}, $project);

    while (-d $path_project)
    {
        $path_project .= '-X';
        $project .='-X';
    }

    dircopy ($path_local, $path_project);

    my $olddir = cwd;
    for my $plot ($self->plots ($project))
    {
        my $path_plot = catdir ($path_project, $plot);
        chdir $path_plot;
        rename ('init.dom', 'init.dom_');
        unlink (glob ('*.score'), glob ('*.fails'), glob ('*.dom'));
        rename ('init.dom_', 'init.dom');
    }
    chdir $olddir;
    return $project;
}

=item occlusion_field

Calculate an occlusion field for a specified plot.
Uses buildings from */best.dom and misc *.walls files in project root.
Updates '01/occlusion.field' file.

  $homemaker->occlusion_field ('my project', '01');

=cut

sub occlusion_field
{
    my $self = shift;
    my $project = shift;
    my $plot = shift;

    my $path_project = catdir ($self->{path_data}, $project);

    my @plots = $self->plots ($project);
    @plots = grep {!/^$plot$/x} @plots;
    my @path_dom = map {catfile ($path_project, $_, 'best.dom')} @plots;

    my @path_walls = $self->walls ($project);

    my $olddir = cwd;
    chdir catdir ($path_project, $plot);
    system ('urb-dom2field.pl', catfile ($path_project, $plot, 'init.dom'), @path_dom, @path_walls);
    chdir $olddir;
    return;
}

=item evolve

Create a folder for evolution, initialise it, run the job with specified number of iterations, return the result and delete the folder:

  $homemaker->evolve ('my project', '01', 1024);

=cut

sub evolve
{
    my $self = shift;
    my $project = shift;
    my $plot = shift;
    my $number_iterations = shift || 128;
    my $timeout = shift || 14400;
    $timeout += int (rand 120);
    $timeout += time;
    my $results;

    my $path_temp = $self->path_temp;
    dircopy (catdir ($self->{path_data}, $project, $plot), $path_temp);

    # urb-evolve looks in the parent folder for possible project level config, but here we are
    # evolving a plot by itself in a tempdir, so merge project level config if it exists
    my $path_project = catdir ($self->{path_data}, $project);
    if (-e catfile ($path_project, 'patterns.config'))
    {
        my $config = YAML::LoadFile (catfile ($path_project, 'patterns.config'));
        if (-e catfile ($path_temp, 'patterns.config'))
        {
            my $config_temp = YAML::LoadFile (catfile ($path_temp, 'patterns.config'));
            for my $key (keys %{$config_temp})
            {
                $config->{$key} = $config_temp->{$key};
            }
        }
        YAML::DumpFile (catfile ($path_temp, 'patterns.config'), $config);
    }
    if (-e catfile ($path_project, 'costs.config'))
    {
        my $config = YAML::LoadFile (catfile ($path_project, 'costs.config'));
        if (-e catfile ($path_temp, 'costs.config'))
        {
            my $config_temp = YAML::LoadFile (catfile ($path_temp, 'costs.config'));
            for my $key (keys %{$config_temp})
            {
                $config->{$key} = $config_temp->{$key};
            }
        }
        YAML::DumpFile (catfile ($path_temp, 'costs.config'), $config);
    }

    my $olddir = cwd;
    chdir $path_temp;

    rename ('init.dom', 'init.dom_');
    unlink (glob ('*.score'), glob ('*.fails'), glob ('*.dom'));
    rename ('init.dom_', 'init.dom');

    system 'sync';

    my $ae = eval
    {
        require Algorithm::Evolutionary;
        1;
    };

    if ($ae and (!$ENV{DISABLE_AE}))
    {
        $ENV{MAX_ITERATIONS} = $number_iterations;
        $ENV{HOMEMAKER_TIMEOUT} = $timeout;
        system ('urb-evolve.pl', 'init.dom');
        system ('urb-fitness.pl', glob ('*.dom'));
        $results->{iterations} = $number_iterations;
    }
    else
    {
    for my $i (1 .. $number_iterations)
    {
        next if time() > $timeout;
        system ('urb-multiply.pl');
        system ('urb-fitness.pl', glob ('*.dom'));
        system ('urb-reap.pl');
        $results->{iterations} = $i;
    }
    }
   
    my ($path_best, $score_best) = $self->best ($path_temp);
    $results->{score} = $score_best;

    chdir (catfile ($self->{path_data}, $project, $plot));

    copy ($path_best, time().'.'.hostname().'.'.$$.'.dom');
    $ENV{FORCE_UPDATE} = 'TRUE';
    system ('urb-fitness.pl', glob ('*.dom'));
    ($path_best, $score_best) = $self->best (cwd());
    copy ($path_best, 'best.dom');
    system ('urb-fitness.pl', 'best.dom');
    system 'sync';

    chdir $olddir;
    return $results;
}

=item molior ifc dxf collada stl

Generate L<Molior> files from 'best' buildings in the project, put them in a temporary tree:

  $path_temp_molior = $homemaker->molior ('my project');

Generate an IFC file from a tree of molior files:

  $path_ifc = $homemaker->ifc ($path_temp_molior);

Generate a DXF file from a tree of molior files:

  $path_dxf = $homemaker->dxf ($path_temp_molior);

Generate a DAE Collada file from the DXF file in the same folder:

  $path_dae = $homemaker->collada ($path_temp_molior);

Generate a STL Stereolithography file from the DXF file in the same folder:

  $path_dae = $homemaker->stl ($path_temp_molior);

=cut

sub molior
{
    my $self = shift;
    my $project = shift;

    my $path_project = catdir ($self->{path_data}, $project);
    my $path_temp = $self->path_temp;

    my @plots = $self->plots ($project);
    my $olddir = cwd;
    for my $plot (@plots)
    {
        mkdir catdir ($path_temp, $plot);
        chdir catdir ($path_temp, $plot);
        system ('urb-dom2molior.pl', catfile ($path_project, $plot, 'best.dom'), 'best.molior');
    }
    chdir $path_project;
    for my $wall (glob '*.walls')
    {
        copy ($wall, $path_temp);
    }
    chdir $olddir;
    return $path_temp;
}

sub ifc
{
    my $self = shift;
    my $path_temp = shift;

    my $olddir = cwd;
    chdir ($path_temp);
    system ('molior-ifc.pl', glob ('*/best.molior'));
    chdir $olddir;

    return catfile ($path_temp, 'molior.ifc');
}

sub dxf
{
    my $self = shift;
    my $path_temp = shift;

    my $olddir = cwd;
    chdir ($path_temp);
    for my $path_wall (glob ('*.walls'))
    {
        system ('walls23dface.pl', $path_wall, $path_wall.'.dxf');
        system ('3dface2polyline.pl', $path_wall.'.dxf', $path_wall.'.dxf');
    }
    system ('molior.pl', glob ('*/best.molior'));
    system ('dxfcat.pl', glob ('*.walls.dxf'), 'molior.dxf', 'molior.dxf');
    chdir $olddir;

    return catfile ($path_temp, 'molior.dxf');
}

sub collada
{
    my $self = shift;
    my $path_temp = shift;

    my $olddir = cwd;
    chdir ($path_temp);
    system ('polyline2collada.pl', 'molior.dxf', 'molior.dae');
    chdir $olddir;

    return catfile ($path_temp, 'molior.dae');
}

sub stl
{
    my $self = shift;
    my $path_temp = shift;

    my $olddir = cwd;
    chdir ($path_temp);
    system ('dxfbind.pl', 'molior.dxf', 'molior_bound.dxf');
    `polyline2stl.pl molior_bound.dxf > molior.stl`;
    chdir $olddir;

    return catfile ($path_temp, 'molior.stl');
}

=back

=head1 AUTHOR

Bruno Postle <bruno@postle.net>

=head1 LICENSE AND COPYRIGHT

Copyright 2014 Bruno Postle.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see L<http://www.gnu.org/licenses/>.


=cut

1;
