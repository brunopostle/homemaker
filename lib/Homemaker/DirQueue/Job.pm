package Homemaker::DirQueue::Job;

use 5.010;
use strict;
use warnings;
use File::Spec::Functions;
use File::Copy;
use YAML;
use Sys::Hostname;
use Time::HiRes qw /time/;

=head1 NAME

Homemaker::DirQueue::Job - Filesystem queue task

=head1 SYNOPSIS

  use Homemaker::DirQueue::Job;

=head1 METHODS

=over

=item new

Create a new queued Homemaker::DirQueue::Job instance:

  my $job = Homemaker::DirQueue::Job->new ({dir => '/path/to/queue', string => $string, metadata => $metadata}, requirements => []);

=cut

sub new
{
    my $class = shift;
    my $self = shift;

    bless $self, $class;

    $self->set_id;
    YAML::DumpFile ($self->path_new, $self);
    system 'sync';
    return unless -e $self->path_new;

    return $self;
}

=item active

Mark a job as active, returns undef if this failed due to a race condition, job
is already active, job is complete, or if job has unmet requirements:

  $job->active || say 'try something else';

=cut

sub active
{
    my $self = shift;

    return unless -e $self->path_new;

    for my $requirement ($self->get_requirements)
    {
        return unless -e catfile ($self->{dir}, 'done', $requirement);
    }

    return unless File::Copy::move ($self->path_new, $self->path_tmp);
    system 'sync';
    return if -e $self->path_new;

    $self->{active_pid} = $$;
    $self->{active_host} = hostname();
    $self->{time_started} = time();

    YAML::DumpFile ($self->path_active, $self);
    system 'sync';
    unless (-e $self->path_active)
    {
        File::Copy::move ($self->path_tmp, $self->path_new);
        system 'sync';
        return;
    }
    unlink $self->path_tmp;
    return $self;
}

=item finish

Mark a job as finished, returns undef if this failed because job isn't active, or due to a race condition:

  $job->finish || say 'bad things happened';

=cut

sub finish
{
    my $self = shift;

    return unless -e $self->path_active;

    return unless File::Copy::move ($self->path_active, $self->path_tmp);
    system 'sync';
    return if -e $self->path_active;

    $self->{time_finished} = time();
    $self->{hostname_finished} = hostname();

    YAML::DumpFile ($self->path_done, $self);
    system 'sync';
    unless (-e $self->path_done)
    {
        File::Copy::move ($self->path_tmp, $self->path_active);
        system 'sync';
        return;
    }
    unlink $self->path_tmp;
    return $self;
}

=item delete

Remove a job from the queue regardless of completion state, this doesn't kill any active processes.

=cut

sub delete
{
    my $self = shift;

    return unless defined $self->path_wherever and -e $self->path_wherever;

    return unless unlink $self->path_wherever;
    system 'sync';
    return $self;
}

=item resubmit

Take an active job and move it back into the queue, this doesn't actually kill
any active processes although they won't be able to finish() as a result of the
file being deleted.

Returns undef if this failed because job isn't active, or due to a race condition:

=cut

sub resubmit
{
    my $self = shift;

    return unless -e $self->path_active;

    return unless File::Copy::move ($self->path_active, $self->path_tmp);
    system 'sync';
    return if -e $self->path_active;

    delete $self->{active_pid};
    delete $self->{active_host};
    delete $self->{time_started};

    YAML::DumpFile ($self->path_new, $self);
    system 'sync';
    return unless -e $self->path_new;
    unlink $self->path_tmp;
    return $self;
}

=item set_id get_id

Each job has a unique id, which is used as the filename:

  $job->set_id;
  my $id = $job->get_id;

The id is constructed from the submitted machine's hostname, the submitted
process's PID, the epoch time in seconds and a random integer.

=cut

sub set_id
{
    my $self = shift;
    my $hostname = hostname();
    my $pid = $$;
    my $time = time();
    my $rand = int rand (99999999);

    $self->{id} = join '_', $hostname, $pid, $time, $rand;
    $self->{time_submitted} = $time;
    $self->{hostname_submitted} = $hostname;

    return $self->get_id;
}

sub get_id
{
    my $self = shift;
    return $self->{id};
}

=item get_data

Retrieve the job 'data':

  my $string = $job->get_data;

=cut

sub get_data
{
    my $self = shift;
    return $self->{string};
}

=item get_requirements

Retrieve a list of ids of any required jobs preceding this one:

  my @requirements = $job->get_requirements;

=cut

sub get_requirements
{
    my $self = shift;
    $self->{requirements} = [] unless defined $self->{requirements};
    return @{$self->{requirements}};
}

=item elapsed_seconds elapsed_hours timeout_hours

Get time in seconds spent processing this job:

  $seconds = $job->elapsed_seconds;

=cut

sub elapsed_seconds
{
    my $self =shift;
    return ($self->{time_finished} - $self->{time_started}) if defined $self->{time_finished};
    return (time() - $self->{time_started}) if defined $self->{time_started};
    return 0;
}

sub elapsed_hours
{
    my $self = shift;
    return sprintf ("%.2f", $self->elapsed_seconds/3600);
}

sub timeout_hours
{
    my $self = shift;
    return sprintf ("%.2f", $self->{metadata}->{timeout}/3600);
}

=item path_new path_tmp path_active path_done

Get the filesystem path to the job file, depending on where we are:

  my $path_new = $job->path_new;

=item path_wherever

..or get the filesystem path without knowing the current status:

  my $path = $job->path_wherever;

=back

=cut

sub path_new
{
    my $self = shift;
    return catfile ($self->{dir}, 'new', $self->get_id);
}

sub path_tmp
{
    my $self = shift;
    return catfile ($self->{dir}, 'tmp', $self->get_id);
}

sub path_active
{
    my $self = shift;
    return catfile ($self->{dir}, 'active', $self->get_id);
}

sub path_done
{
    my $self = shift;
    return catfile ($self->{dir}, 'done', $self->get_id);
}

sub path_wherever
{
    my $self = shift;
    return $self->path_done if -e $self->path_done;
    return $self->path_active if -e $self->path_active;
    return $self->path_new if -e $self->path_new;
    return $self->path_tmp if -e $self->path_tmp;
    return undef;
}

1;
