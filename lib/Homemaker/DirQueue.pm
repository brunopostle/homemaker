package Homemaker::DirQueue;

use 5.010;
use lib 'lib';
use strict;
use warnings;
use File::Spec::Functions;
use File::Path 'make_path';
use File::Copy;
use YAML;
use Homemaker::DirQueue::Job;

=head1 NAME

Homemaker::DirQueue - Filesystem queue

=head1 SYNOPSIS

Queue manager for L<Urb> loosely based on L<IPC::DirQueue>.

L<IPC::DirQueue> is great and all that, but doesn't work on the simplistic
filesystem offered by Amazon S3, grrr.  This module offers a similar API, but
relies on basic file operations, and is probably not as robust as a result.

It does however have extra functionality: each job can require the completion
of other jobs first.

  use Homemaker::DirQueue;

=head1 METHODS

=over

=item new

Create a new Homemaker::DirQueue instance:

  my $queue = Homemaker::DirQueue->new ({dir => '/path/to/queue'});

=cut

sub new
{
    my $class = shift;
    my $self = shift;

    bless $self, $class;

    mkdir $self->path_new unless -d $self->path_new;
    mkdir $self->path_tmp unless -d $self->path_tmp;
    mkdir $self->path_active unless -d $self->path_active;
    mkdir $self->path_done unless -d $self->path_done;

    return $self;
}

=item wait_for_queued_job

Fetch the next job to be processed, result is either an
L<Homemaker::DirQueue::Job> object or undef if nothing is available right now:

  my $job = $dirqueue->wait_for_queued_job;

This 'active' job is moved from the 'new' unprocessed tasks queue.

=cut

sub wait_for_queued_job
{
    my $self = shift;

    for my $job ($self->jobs ('active'))
    {
        my $timeout = 14400;
        $timeout = $job->{metadata}->{timeout} if defined $job->{metadata}->{timeout};
        next if $job->elapsed_seconds < $timeout + 3600;
        $job->resubmit;
    }

    for my $job ($self->jobs ('new'))
    {
        return $job if $job->active;
    }

    return;
}

=item enqueue_string

Submit a new job to the queue, supplied info is a string, a metadata hash, an
optional list of jobs that need to be complete before this one can start:

  my $job = $queue->enqueue_string ('my_string', {key => 'value'}, @requirements);

=cut

sub enqueue_string
{
    my ($self, $string, $metadata, @requirements) = @_;
    my $job = Homemaker::DirQueue::Job->new ({dir => $self->{dir}, string => $string, metadata => $metadata,
        requirements => [map {$_->get_id} @requirements]});
    return $job;
}

=item by_id

Retrieve a job given just the 'id' string:

  my $job = $queue->by_id ('HOSTNAME_1234_1234567890.12345_12345678');

=cut

sub by_id
{
    my ($self, $id) = @_;

    my $job = {id => $id, dir => $self->{dir}};
    bless $job, 'Homemaker::DirQueue::Job';
    my $path_job = $job->path_wherever || return undef;
    local $YAML::LoadBlessed = 1;
    $job = YAML::LoadFile ($job->path_wherever) || return undef;

    return $job;
}

=item jobs

Get a list of jobs in each state:

  @jobs = $self->jobs ('new');
  @jobs = $self->jobs ('tmp');
  @jobs = $self->jobs ('active');
  @jobs = $self->jobs ('done');

=cut

sub jobs
{
    my $self = shift;
    my $state = shift;

    my $path;
    $path = $self->path_new if $state eq 'new';
    $path = $self->path_tmp if $state eq 'tmp';
    $path = $self->path_active if $state eq 'active';
    $path = $self->path_done if $state eq 'done';
    return () unless $path;

    my @jobs;
    opendir (DIR, $path);
    while (my $name = readdir (DIR))
    {
        #next unless $name =~ /[0-9]/;
        next unless $name =~ /^[[:alnum:]._-]+[_0-9.]+/x;
        next if $name =~ /^\./x;
        my $path_item = catfile ($path, $name);
        next unless -e $path_item;
        local $YAML::LoadBlessed = 1;
        push @jobs, YAML::LoadFile ($path_item);
    }
    closedir DIR;

    return @jobs;
}

=item path_new path_tmp path_active path_done

Access the path to each of the folder used for the queue.  Unprocessed tasks
are placed in the 'new' folder, active tasks are placed in 'active' and
finished tasks in 'done'.

  my $path_new = $queue->path_new;

There is also a 'tmp' folder.

=back

=cut

sub path_new
{
    my $self = shift;
    return catdir ($self->{dir}, 'new');
}

sub path_tmp
{
    my $self = shift;
    return catdir ($self->{dir}, 'tmp');
}

sub path_active
{
    my $self = shift;
    return catdir ($self->{dir}, 'active');
}

sub path_done
{
    my $self = shift;
    return catdir ($self->{dir}, 'done');
}

1;
