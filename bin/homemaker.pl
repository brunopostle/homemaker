#!/usr/bin/perl

use strict;
use warnings;
use 5.010;
use lib 'lib';
use Carp;
use Homemaker;
use Homemaker::DirQueue;

croak 'HOMEMAKER_QUEUE undefined' unless defined $ENV{HOMEMAKER_QUEUE};
croak 'HOMEMAKER_DATA undefined'  unless defined $ENV{HOMEMAKER_DATA};

my $homemaker = Homemaker->new || croak;
my $dirqueue = Homemaker::DirQueue->new ({dir => $homemaker->{path_queue}});

local $SIG{INT} = \&interrupt;
local $SIG{TERM} = \&interrupt;

sleep rand (5);

while (1)
{
    say "[$$] Waiting for job...";
    my $job = $dirqueue->wait_for_queued_job;

    if ($job and $job->get_data eq 'evolve')
    {
        my $project = $job->{metadata}->{project};
        my $plot    = $job->{metadata}->{plot};
        my $generation = $job->{metadata}->{generation};
        my $number_iterations = $job->{metadata}->{number_iterations};
        my $timeout = $job->{metadata}->{timeout} || $homemaker->timeout;
        say "[$$] Evolution started: $project/$plot; generation=$generation; timeout=$timeout";

        $homemaker->occlusion_field ($project, $plot);
        my $results = $homemaker->evolve ($project, $plot, $number_iterations, $timeout);
        for my $key (keys %{$results}) {$job->{metadata}->{$key} = $results->{$key}};
        say "[$$] Evolution finished: $project/$plot; generation=$generation; timeout=$timeout";

        $job->resubmit unless $results;
        $job->finish || $job->resubmit;
    }
    elsif ($job and $job->get_data eq 'end')
    {
        my $project = $job->{metadata}->{project};

        for my $id_requirement ($job->get_requirements)
        {
            my $job_done = $dirqueue->by_id ($id_requirement) || next;
            $job_done->delete;
        }
        $job->finish;
        say "[$$] Project completed: $project";
    }
    else
    {
        say "[$$] sleep 60";
        sleep 60;
    }
}

sub interrupt
{
    say STDERR "[$$] sending SIG_INT";
    local $SIG{INT} = "IGNORE";
    kill("-INT", $$);
    sleep 5;
    exit 0;
}

0;

__END__

=head1 NAME

homemaker - daemon for dequeueing Homemaker projects

=head1 SYNOPSIS

export HOMEMAKER_QUEUE=/path/to/queue
export HOMEMAKER_DATA=/path/to/data

homemaker.pl

=head1 DESCRIPTION

B<homemaker> processes a queue of L<Homemaker> jobs.

Uses command-line tools from L<Urb>.

Can be run multiple times in parallel, and on multiple machines as long as the
queue and data folders are shared and writable.

=head1 COPYRIGHT

Copyright (c) 2014 Bruno Postle <bruno@postle.net>.

This file is part of Homemaker.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see L<http://www.gnu.org/licenses/>.

=head1 SEE ALSO

L<Urb> L<Homemaker> L<Homemaker::DirQueue>

=cut

