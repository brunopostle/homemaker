#!/usr/bin/perl

use strict;
use warnings;
use 5.010;
use lib 'lib';
use Carp;
use Homemaker;
use Homemaker::DirQueue;
use File::Copy;
use File::Spec::Functions qw/ catdir /;

croak 'HOMEMAKER_QUEUE undefined' unless defined $ENV{HOMEMAKER_QUEUE};
croak 'HOMEMAKER_DATA undefined'  unless defined $ENV{HOMEMAKER_DATA};

my $homemaker = Homemaker->new || croak;
my $project = shift @ARGV || croak 'Specify a project name';
my @plots = $homemaker->plots ($project);
croak "No project named '$project' found" unless scalar @plots;

my $dirqueue = Homemaker::DirQueue->new ({dir => $homemaker->{path_queue}});

# get all the best scores for each plot in the project
my $lookup = {};
for my $plot (@plots)
{
    my $path_plot = catdir ($homemaker->{path_data}, $project, $plot);
    my ($path_best, $score_best) = $homemaker->best ($path_plot);
    $lookup->{$score_best} = $plot;
}

# add all the scores together so we can figure out how much work they need relative to each other
my $total = 0;
for my $score (sort keys %{$lookup})
{
    next unless $score;
    $total += 1/sqrt($score);
    say join ' ', $lookup->{$score}, $score;
}

# collect all the jobs so we can cleanup later with the 'end' job
my @requirements_project;

# each plot has an existing 'score'
for my $score (sort keys %{$lookup})
{
    next unless $score;
    my $plot = $lookup->{$score};
    my $number_generations = int (scalar (@plots) * $homemaker->number_generations / 2 / sqrt($score) / $total);

    my @requirements;

    for my $generation (1 .. $number_generations)
    {
        push @requirements, $dirqueue->enqueue_string ('evolve',
           {project => $project, plot => $plot, generation => $generation.'/'.$number_generations,
            number_iterations => $homemaker->number_iterations, timeout => $homemaker->timeout}, ());

        say "Evolution queued: $project/$plot; generation=$generation/$number_generations; iterations=".$homemaker->number_iterations."; timeout=".$homemaker->timeout;

        push @requirements_project, @requirements;
    }
}

$dirqueue->enqueue_string ('end', {project => $project, number_generations => 1, number_iterations => 1}, @requirements_project);

0;

__END__

=head1 NAME

homemaker-redo - apply more evolution to an existing project

=head1 SYNOPSIS

export HOMEMAKER_QUEUE=/path/to/queue
export HOMEMAKER_DATA=/path/to/data

homemaker-redo.pl project_name

=head1 DESCRIPTION

B<homemaker-redo> takes a queued or finished L<Homemaker> project and adds more evolution
iterations, it looks at the existing scores and gives more time to low scoring plots

=head1 COPYRIGHT

Copyright (c) 2020 Bruno Postle <bruno@postle.net>.

This file is part of Homemaker.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see L<http://www.gnu.org/licenses/>.

=head1 SEE ALSO

L<Urb> L<Molior> L<File::DXF> L<File::IFC> L<Homemaker>

=cut

