#!/bin/sh

# run Homemaker in parallel

export PATH=$PATH:$HOME/src/file-dxf/bin:$HOME/src/file-ifc/bin:$HOME/src/homemaker/bin:$HOME/src/molior/bin:$HOME/src/urb/bin
export PERL5LIB=$PERL5LIB:$HOME/src/file-dxf/lib:$HOME/src/file-ifc/lib:$HOME/src/homemaker/lib:$HOME/src/molior/lib:$HOME/src/urb/lib

export HOMEMAKER_DATA=$HOME/homemaker/data/
export HOMEMAKER_QUEUE=$HOME/homemaker/queue/

grep processor /proc/cpuinfo | while read CPU; do (homemaker.pl &); sleep 10; done

