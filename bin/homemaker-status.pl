#!/usr/bin/perl

use strict;
use warnings;
use 5.010;
use lib 'lib';
use Carp;
use Homemaker;
use Homemaker::DirQueue;
use Cwd;

croak 'HOMEMAKER_QUEUE undefined' unless defined $ENV{HOMEMAKER_QUEUE};
croak 'HOMEMAKER_DATA undefined'  unless defined $ENV{HOMEMAKER_DATA};

my $homemaker = Homemaker->new || croak;
my $dirqueue = Homemaker::DirQueue->new ({dir => $homemaker->{path_queue}});

say '###### QUEUED JOBS ######';
for my $job (sort {$a->{time_submitted} <=> $b->{time_submitted}} $dirqueue->jobs ('new'))
{
    my $meta = $job->{metadata};
    if ($job->get_data eq 'evolve')
    {
    print $meta->{project}.'/'.$meta->{plot}.', ';
    print $meta->{number_iterations}.', ';
    print $meta->{generation}.', ';
    print localtime ($job->{time_submitted}).', ';
    print $job->{hostname_submitted}."\n";
    }
    elsif ($job->get_data eq 'start')
    {
    print $meta->{project}.', ';
    print $meta->{number_iterations}.' iterations, ';
    print $meta->{number_generations}.' generations, ';
    print localtime ($job->{time_submitted})."\n";
    }
}

say '###### ACTIVE JOBS ######';
for my $job (sort {$a->{time_submitted} <=> $b->{time_submitted}} $dirqueue->jobs ('active'))
{
    next unless $job->get_data eq 'evolve';
    my $meta = $job->{metadata};
    print $meta->{project}.'/'.$meta->{plot}.', ';
    print $meta->{number_iterations}.', ';
    print $meta->{generation}.', ';
    print $job->elapsed_hours.'/'.$job->timeout_hours.' hours, ';
    print $job->{active_host}.'/'.$job->{active_pid}."\n";
}

say '###### FINISHED JOBS ######';
for my $job (sort {$a->{time_submitted} <=> $b->{time_submitted}} $dirqueue->jobs ('done'))
{
    my $meta = $job->{metadata};
    if ($job->get_data eq 'evolve')
    {
    print $meta->{project}.'/'.$meta->{plot}.', ';
    print sprintf("%.8f", $meta->{score}).', ';
    print $meta->{iterations}.'/'.$meta->{number_iterations}.', ';
    print $meta->{generation}.', ';
    print $job->elapsed_hours.'/'.$job->timeout_hours.' hours, ';
    print localtime ($job->{time_finished}).', ';
    print $job->{hostname_finished}."\n";
    }
    elsif ($job->get_data eq 'end')
    {
    print $meta->{project}.', ';
    print $meta->{number_iterations}.' iterations, ';
    print $meta->{number_generations}.' generations, ';
    print localtime ($job->{time_submitted}).', ';
    print localtime ($job->{time_finished})."\n";
    }
}

0;
