#!/usr/bin/perl

use strict;
use warnings;
use 5.010;
use lib 'lib';
use Carp;
use Homemaker;
use Homemaker::DirQueue;

croak 'HOMEMAKER_QUEUE undefined' unless defined $ENV{HOMEMAKER_QUEUE};
croak 'HOMEMAKER_DATA undefined'  unless defined $ENV{HOMEMAKER_DATA};

my $homemaker = Homemaker->new || croak;
my $dirqueue = Homemaker::DirQueue->new ({dir => $homemaker->{path_queue}});

for my $path_local (@ARGV)
{
    croak "$path_local doesn't exist" unless -d $path_local;
    my $project = $homemaker->prep ($path_local);
    croak "$path_local already queued" unless defined $project;

    my @plots = $homemaker->plots ($project);

    my $number_generations = $homemaker->number_generations;
    my $number_iterations = $homemaker->number_iterations;
    my $timeout = $homemaker->timeout;
    my @requirements_project;
    my @requirements;
    for my $generation (1 .. $number_generations)
    {
        my $timeout_temp = int ($timeout * $generation / $number_generations);
        my $number_iterations_temp = int ($number_iterations * $generation / $number_generations);
        $number_iterations_temp = 1 if $number_iterations_temp < 1;
        my @generation;
        for my $plot (@plots)
        {
            my @requirements_tmp;
            for my $requirement (@requirements) { push (@requirements_tmp, $requirement) unless $requirement->{metadata}->{plot} eq $plot};
            push @generation, $dirqueue->enqueue_string ('evolve',
                {project => $project, plot => $plot, generation => $generation.'/'.$number_generations,
                 number_iterations => $number_iterations_temp, timeout => $timeout_temp}, @requirements_tmp);
            say "[$$] Evolution queued: $project/$plot; generation=$generation/$number_generations; timeout=$timeout_temp";
        }
        @requirements = @generation;
        push @requirements_project, @requirements;
    }
    $dirqueue->enqueue_string ('end', {project => $project, number_generations => 1, number_iterations => 1}, @requirements_project);
}

0;

__END__

=head1 NAME

homemaker-enqueue - submit Homemaker projects

=head1 SYNOPSIS

  export HOMEMAKER_QUEUE=/path/to/queue
  export HOMEMAKER_DATA=/path/to/data
  homemaker-enqueue.pl /path/to/project_a [...]

=head1 DESCRIPTION

B<homemaker-enqueue> submits one or more projects for processing as L<Homemaker> jobs.

This tool returns immediately, the queue itself is processed by one or more B<homemaker> daemons.

The amount of processing can be adjusted before enqueueing:

  NUMBER_GENERATIONS=16
  NUMBER_ITERATIONS=128

=head1 COPYRIGHT

Copyright (c) 2014 Bruno Postle <bruno@postle.net>.

This file is part of Homemaker.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see L<http://www.gnu.org/licenses/>.

=head1 SEE ALSO

L<Urb> L<Homemaker>

=cut

