#!/usr/bin/perl

use strict;
use warnings;
use 5.010;
use lib 'lib';
use Carp;
use Homemaker;
use File::Copy;
use File::Spec::Functions qw/ catdir /;

croak 'HOMEMAKER_QUEUE undefined' unless defined $ENV{HOMEMAKER_QUEUE};
croak 'HOMEMAKER_DATA undefined'  unless defined $ENV{HOMEMAKER_DATA};

my $homemaker = Homemaker->new || croak;
my $name = shift @ARGV || croak 'Specify a project name and output filename';
croak "No project named '$name' found" unless scalar $homemaker->plots ($name);
croak 'No output filename specified' unless scalar @ARGV;

my $path_temp_molior = $homemaker->molior ($name);

for my $path_output (@ARGV)
{
    if ($path_output =~ /\.ifc$/xi)
    {
        copy ($homemaker->ifc ($path_temp_molior), $path_output);
    }
    elsif ($path_output =~ /\.dxf$/xi)
    {
        copy ($homemaker->dxf ($path_temp_molior), $path_output);
    }
    elsif ($path_output =~ /\.dae$/xi)
    {
        $homemaker->dxf ($path_temp_molior);
        copy ($homemaker->collada ($path_temp_molior), $path_output);
    }
    elsif ($path_output =~ /\.stl$/xi)
    {
        $homemaker->dxf ($path_temp_molior);
        copy ($homemaker->stl ($path_temp_molior), $path_output);
    }
    elsif ($path_output =~ /\.molior\.zip$/xi)
    {
        system ('zip', '-r', $path_output, $path_temp_molior);
    }
    elsif ($path_output =~ /\.zip$/xi)
    {
        my $path_project = catdir ($homemaker->{path_data}, $name);
        system ('zip', '-r', $path_output, $path_project);
    }
    else {croak 'Specify a file with .ifc, .dxf, .dae, .stl, .molior.zip or .zip suffix'}
}

0;

__END__

=head1 NAME

homemaker-export - export 3d geometry

=head1 SYNOPSIS

export HOMEMAKER_QUEUE=/path/to/queue
export HOMEMAKER_DATA=/path/to/data

homemaker-export.pl project_name /path/to/project.ifc [/other/project.dxf] [project.dae]

=head1 DESCRIPTION

B<homemaker-export> takes a queued or finished L<Homemaker> project and exports
3d geometry in one or more formats.

Uses command-line tools from L<Molior>, L<File::DXF>, and L<File::IFC>.

=head1 COPYRIGHT

Copyright (c) 2014 Bruno Postle <bruno@postle.net>.

This file is part of Homemaker.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see L<http://www.gnu.org/licenses/>.

=head1 SEE ALSO

L<Urb> L<Molior> L<File::DXF> L<File::IFC> L<Homemaker>

=cut

