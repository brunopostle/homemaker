---
title: 'Homemaker: software for adaptive domestic design'
tags:
  - Adaptive Design
  - Genetic Algorithms
  - Software Evolution
  - Pattern Languages
  - Perl
  - AEC (Architecture Engineering Construction)
  - BIM (Building Information Modelling)
authors:
  - name: Bruno Postle
    orcid: 0000-0001-9762-276X
    affiliation: 1
affiliations:
  - name: Independent Researcher, UK
    index: 1
date: 21 March 2024
bibliography: paper.bib
---

# Summary

The Homemaker stack of software evolves domestic buildings using a modified version of Christopher Alexander's Pattern Language as fitness criteria.
Multiple buildings can be evolved in parallel, buildings with multiple-storeys, non-orthogonal plots, and courtyard layouts are supported.
The software shows how evolutionary computation provides a design method complementary to the theory of Pattern Languages, thereby addressing a major criticism of Pattern Languages that they do not offer a practical design method.

# Statement of need

Christopher Alexander's work, in particular 'A Pattern Language' [@alexander_pattern_1977], is a description of a saner, safer, more humane built environment, that has had profound influence outside the world of architecture in the form of Software Design Patterns [@postle_pattern_2019].
'A Pattern Language' is a best-seller in architecture, with many advocates, but very few people appear to actually be using it to design buildings.
Pattern Languages for buildings are a failure, even Alexander said it was "my biggest failure" [@hopkins_interview_2010].
The hypothesis behind this software is that the theory behind Pattern Languages is sound, but that there is something missing in terms of a design method, which Alexander identified as iteration, adaptation through lots of small steps, or unfolding, but didn't offer any practical advice beyond that, see 'The Timeless Way of Building' [@alexander_timeless_1979].
Pattern Languages are a useful technology, and the success of Design Patterns in the field of software is evidence of this, but a practical means of implementing A Pattern Language for buildings is elusive.
Homemaker generates 3D BIM (Building Information Models) in validated IFC format, ie. it *can* be used in a workflow to design actual buildings, however the research purpose is to provide a framework where the Pattern Language can be judged as a design theory on its own merits - on the basis that a theory of architecture should be sufficient to actually design buildings that people would want to live in, and that meet human needs.
There are apparently no other tools with a similar aim or scope - 'AI' architectural design is a growing field [@caetano_computational_2020], but the implicit aim of the discipline is improved efficiency of commercial property development.

Homemaker was designed for researchers to use to produce building designs, but with the ability to tweak individual patterns and observe the effects.

# Brief description

The Homemaker system [@postle_adaptive_2013] consists of a collection of software components intended to be run unattended.
The system evolves multi-storey building designs using a Pattern Language [@alexander_pattern_1977] as fitness criteria for selection in a Genetic Algorithm.

The presumed construction process of a historic vernacular building is that it starts with a single room and grows by accretion and subdivision, with periodic removal of failed and unwanted parts of the structure.
To simulate this, a 'load-bearing wall' form-language is used: buildings are nested agglomerations of rooms; each room is a four-sided quadrilateral (not necessarily orthogonal); walls are vertical; the geometry of storeys below informs the geometry of storeys above.
A novel binary tree representation, with each division holding orientation and ratio numbers indicating a geometric partition of a 'room' into two smaller 'rooms', can be used to describe such a building.
The graph/tree is recursive, so any subdivision of rooms and storeys can be represented.
Each leaf-node in the tree is a room with an interchangeable 'usage' (kitchen, bedroom etc..) that can explored by optimisation.
Typical historic vernacular buildings closely fit this model and can usually be represented with such a binary tree.

A specific advantage of a binary tree data structure is that branches can be 'grafted' from one model to another, an analogue of crossover or recombination in sexual reproduction of biological systems - this tree grafting is a standard technique in Genetic Algorithms.
The fitness function consists of selected patterns from the Pattern Language assessing the model in the context of its local environment (the daylight field formed by surrounding buildings).
The fitness calculation ultimately maximises a single value derived from this 'quality', multiplied by floor area, and divided by a simplified cost function representing a physical quantity of building materials.
A population of models, each a variation of the same building, is produced through mutation, crossover and culling.
When multiple buildings are evolved in parallel, the daylight field environment is periodically updated based on the geometry of the buildings as they change.

The software stack consists of: [Homemaker](https://bitbucket.org/brunopostle/homemaker), a queue manager for evolving multiple buildings in parallel; [Urb](https://bitbucket.org/brunopostle/urb), the data model for a single building; and [Molior](https://bitbucket.org/brunopostle/molior), [File::IFC](https://bitbucket.org/brunopostle/file-ifc) and [File::DXF](https://bitbucket.org/brunopostle/file-dxf), which are used to generate a 3D BIM model on completion of the evolution.

# Updates

Recent updates to the software include:

* 'Sahn' Space Type, a courtyard circulation space prevalent in traditional Arab houses [@hakim_arabic-islamic_2013]
  
* A configuration system allows users to tailor pattern fitness parameters and building costs for individual and groups of buildings.

* The Algorithm::Evolutionary [@merelo_guervos_algorithmevolutionary_2010] Perl module replaces the previous Makefile-based system for driving evolution in populations.

* The Homemaker queue manager allocates resources for the parallel evolution of multiple buildings.

* Space centrality is calculated using the average path length over the circulation graph to all other spaces.

# Challenges

This software is fundamentally non-interactive.
User control is limited to the setting initial parameters, though the resulting IFC BIM model is readily editable in Native IFC software such as BlenderBIM [@moult_blenderbim_2023].
Homemaker uses a binary tree to describe the building, but this is conceptually hard for a human to manipulate directly as it has no visual relation to a building design.
The data structure is instructions to build (a genotype), rather than the finished geometry (a phenotype).
A separate but related project not described here is the 'Homemaker add-on' [@postle_homemaker_2023] that provides an entirely interactive experience based on the same principles, using non-manifold spatial geometry and the Topologic library [@eloy_topologic_2021].

Evolutionary design in this manner seems to be unsuitable for placing smaller buildings on larger plots, or larger 'towers in a park' designs, the constraint of occupying the entire space appears to be necessary.
For a more suburban typology, pre-prepared standard designs distributed in a 'cookie-cutter' manner would likely achieve the desired result more efficiently.
Homemaker is also relentlessly domestic, producing good ordinary buildings, these buildings don't show the hand of an architect, and as such may not be perceived as Architecture.

# Acknowledgements

Christopher Alexander (1936-2022) was the inspiration for this work.

# References
