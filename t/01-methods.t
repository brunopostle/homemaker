#!/usr/bin/perl
use 5.010;
use strict;
use warnings FATAL => 'all';
use Test::More 'no_plan';
use File::Temp 'tempdir';
use lib 'lib';

BEGIN {
    use_ok( 'Homemaker' ) || print "Bail out!\n";
}

my $temp_folder = tempdir (CLEANUP => 1);

undef $ENV{HOMEMAKER_DATA};
undef $ENV{HOMEMAKER_QUEUE};

ok (!Homemaker->new, 'new() fails');

$ENV{HOMEMAKER_QUEUE} = "$temp_folder/queue";

ok (!Homemaker->new, 'new() fails');

$ENV{HOMEMAKER_DATA} = "$temp_folder/data";

ok (Homemaker->new, 'new() success');
my $homemaker = Homemaker->new;

my $path_temp = $homemaker->path_temp;
ok (-d $path_temp, 'path_temp()');

$ENV{NUMBER_GENERATIONS} = 10;
my $num = $homemaker->number_generations;
is ($num, 10, 'number_generations()');
undef $ENV{NUMBER_GENERATIONS};
$num = $homemaker->number_generations;
is ($num, 16, 'number_generations()');

$ENV{NUMBER_ITERATIONS} = 1024;
$num = $homemaker->number_iterations;
is ($num, 1024, 'number_iterations()');
undef $ENV{NUMBER_ITERATIONS};
$num = $homemaker->number_iterations;
is ($num, 768, 'number_iterations()');

ok (my $project = $homemaker->prep ('t/data/project_test'), 'prep()');
is ($project, 'project_test', 'prep()');
is ($homemaker->prep ('t/data/project_test'), 'project_test-X', 'prep() fails');

my @plots = $homemaker->plots ('project_test');
is (scalar @plots, 2, 'two plots');
like ($plots[0], '/^[ab]$/', 'plots');
like ($plots[1], '/^[ab]$/', 'plots');

my @walls = $homemaker->walls ('project_test');
is (scalar @walls, 1, 'walls()');
ok (-e $walls[0], 'walls()');

my ($path_dom, $score_dom) = $homemaker->best ('t/data/project_test/a');
like ($path_dom, '/t\/data\/project_test\/a/', 'best()');
ok (-e $path_dom, 'best()');
like ($path_dom, '/init.dom$/', 'best()');

# $homemaker->evolve ('project_test', 'a', 768);

