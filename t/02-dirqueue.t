#!perl
use 5.010;
use strict;
use warnings FATAL => 'all';
use Test::More 'no_plan';
use File::Temp 'tempdir';
use lib 'lib';

BEGIN {
    use_ok( 'Homemaker::DirQueue' ) || print "Bail out!\n";
}

my $path_queue = tempdir (CLEANUP => 1);

my $dirqueue = Homemaker::DirQueue->new ({dir => $path_queue});

my $dirqueue2 = Homemaker::DirQueue->new ({dir => $path_queue});

my $job_new = $dirqueue->enqueue_string ('my_string', {key => 'value'});

ok ($job_new->get_id =~ /[0-9]/x, 'get_id()');

is ($job_new->path_new, $job_new->path_wherever, 'path_wherever()');

my $job = $dirqueue->wait_for_queued_job;

ok (-e $job->path_active, 'path_active()');
is ($job->path_active, $job->path_wherever, 'path_wherever()');

is ($job->active, undef, 'active job can\'t be made active');

ok (ref $job eq 'Homemaker::DirQueue::Job', 'blessed');

ok ($job->get_data eq 'my_string', 'get_data()');

ok ($job->finish, 'finish()');
ok (!$job->finish, 'finished job can\'t be finished');

ok (-e $job->path_done, 'path_done()');
is ($job->path_done, $job->path_wherever, 'path_wherever()');

ok (!-e $job->path_tmp, 'deletion');
ok (!-e $job->path_new, 'deletion');
ok (!-e $job->path_active, 'deletion');
ok (-e $job->path_done, 'deletion');

my $by_id = $dirqueue->by_id ($job->get_id);
is_deeply ($by_id, $job, 'by_id()');

my $by_dud_id = $dirqueue->by_id ('some_made_up_string');
is ($by_dud_id, undef, 'by_id() with invalid id returns undef');

# dependency

my $job1 = $dirqueue->enqueue_string ('job1', {key => 'value'});
my $job2 = $dirqueue->enqueue_string ('job2', {key => 'value'}, $job1);
my $job3 = $dirqueue->enqueue_string ('job3', {key => 'value'}, $job2, $job1);
my $job4 = $dirqueue->enqueue_string ('job4', {key => 'value'}, $job3);

is ((scalar $job1->get_requirements), 0, '0 requirements');
is ((scalar $job2->get_requirements), 1, '1 requirements');
is ((scalar $job3->get_requirements), 2, '2 requirements');
is ((scalar $job4->get_requirements), 1, '1 requirements');

my $job_a = $dirqueue->wait_for_queued_job;
is ($job_a->get_data, $job1->get_data, 'job1');
$job_a->finish;

my $job_b = $dirqueue->wait_for_queued_job;
is ($job_b->get_data, $job2->get_data, 'job2');
$job_b->finish;

my $job_c = $dirqueue->wait_for_queued_job;
is ($job_c->get_data, $job3->get_data, 'job3');
#$job_c->finish;

my $job_d = $dirqueue->wait_for_queued_job;
is ($job_d, undef, 'nothing to do');

$job_c->finish;

my $job_e = $dirqueue->wait_for_queued_job;
is ($job_e->get_data, $job4->get_data, 'job4');
$job_e->finish;

my $job_f = $dirqueue->wait_for_queued_job;
is ($job_f, undef, 'nothing to do');

# resubmit

my $job5 = $dirqueue->enqueue_string ('job5', {key => 'value'});
ok (-e $job5->path_new, 'path_new()');

ok ($job5->elapsed_seconds == 0, 'elapsed_seconds()');

my $job_g = $dirqueue->wait_for_queued_job;
ok (-e $job_g->path_active, 'path_active()');
ok (!-e $job_g->path_new, 'path_new()');

sleep 2;

ok ($job_g->elapsed_seconds > 0, 'elapsed_seconds()');
like ($job_g->elapsed_hours, '/[0-9]\.[0-9]+/', 'elapsed_hours()');

$job_g->resubmit;
ok (!-e $job_g->path_active, 'path_active()');
ok (-e $job_g->path_new, 'path_new()');

ok (!$job_g->resubmit, 'resubmit()');
my $job_h = $dirqueue->wait_for_queued_job;

sleep 2;

$job_h->finish;
ok ($job_h->elapsed_seconds > 0, 'elapsed_seconds()');

ok (-e $job_h->path_done, 'finished job in \'done\' queue');
ok ($job_h->delete, 'deleting a finished job succeeds');
ok (!$job_h->path_wherever, 'deleted job can\'t be found');

ok (!$job_h->delete, 'deleting a deleted job fails');

0;
