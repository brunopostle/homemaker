# Installation

### Debian/Ubuntu/Raspberry PI

Install some standard deb packages

```
sudo apt-get install \
  git \
  libalgorithm-permute-perl \
  libbit-vector-perl \
  libdatetime-perl libgraph-perl libmath-polygon-perl \
  libfile-copy-recursive-perl \
  libfile-sharedir-install-perl \
  libfile-sharedir-perl \
  libgd-perl \
  liblib-abs-perl \
  liblocal-lib-perl \
  libmath-matrixreal-perl \
  libmath-random-free-perl \
  libmemory-usage-perl \
  libmodule-pluggable-perl \
  libsort-key-perl \
  libstatistics-basic-perl \
  libstring-random-perl \
  libsvg-perl \
  libtest-pod-coverage-perl \
  libtree-dagnode-perl \
  libxml-parser-easytree-perl \
  libxml-parser-perl \
  libyaml-perl
```

The rest of the prerequisites are not available through Debian and need to be
installed through CPAN. Accept all defaults. You may need to log-out and log-in
again after this:

```
cpan Algorithm::Evolutionary
```

### Fedora

Install some standard Fedora packages

```
sudo dnf install \
  cpan \
  git \
  'perl(Bit::Vector)' \
  'perl(DateTime)' \
  'perl(ExtUtils::MakeMaker)' \
  'perl(ExtUtils::PkgConfig)' \
  'perl(File::Copy::Recursive)' \
  'perl(File::ShareDir::Install)' \
  'perl(File::Slurp::Tiny)' \
  'perl(GD)' \
  'perl(Graph)' \
  'perl(lib::abs)' \
  'perl(local::lib)' \
  'perl(Math::Polygon)' \
  'perl(Math::MatrixReal)' \
  'perl(Module::Pluggable)' \
  'perl(Number::Format)' \
  'perl(Sort::Key)' \
  'perl(Statistics::Basic)' \
  'perl(String::Random)' \
  'perl(SVG)' \
  'perl(Test::LeakTrace)' \
  'perl(Test::Pod)' \
  'perl(Test::Pod::Coverage)' \
  'perl(Tree::DAG_Node)' \
  'perl(XML::Parser)' \
  'perl(YAML)'
```

The rest of the prerequisites are not available through Fedora and need to be
installed through CPAN. Accept all defaults. You may need to log-out and log-in
again after this:

```
cpan Algorithm::Evolutionary Memory::Usage
```

## Install the Homemaker tool stack

Fetch the latest code via git, and install:

```
git clone https://bitbucket.org/brunopostle/urb.git
git clone https://bitbucket.org/brunopostle/file-dxf.git
git clone https://bitbucket.org/brunopostle/file-ifc.git
git clone https://bitbucket.org/brunopostle/molior.git
git clone https://bitbucket.org/brunopostle/homemaker.git

cd urb && perl Makefile.PL && make test && make install; cd -
cd file-dxf && perl Makefile.PL && make test && make install; cd -
cd file-ifc && perl Makefile.PL && make test && make install; cd -
cd molior && perl Makefile.PL && make test && make install; cd -
cd homemaker && perl Makefile.PL && make test && make install; cd -

```

## Configure Homemaker folders

The Homemaker queue manager needs some folders for results and temporary files.
These can be local or on a shared drive. Note that queue processing can be
distributed over multiple machines if these folders are on a shared drive.

For local folders, add these lines to your `~/.bashrc` file (log-out and log-in
afterwards):

```
export HOMEMAKER_DATA="$HOME/homemaker/data"
export HOMEMAKER_QUEUE="$HOME/homemaker/queue"
```

## Quick start

Evolve a single house:

```
cd urb/examples/quarter/00
urb-evolve.pl init.dom
```

Generate some 3D model representations of a single house:

```
dom2dxf.sh filename.dom
```

Evolve a collection of houses:

```
homemaker.pl &
homemaker-enqueue.pl urb/examples/quarter
```

Export an IFC model representation of a collection of evolved houses:

```
homemaker-export.pl quarter filename.ifc
```

Bruno Postle <bruno@postle.net>
April 2024
